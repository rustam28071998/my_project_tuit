package uz.pdp.mailverifationhomework.utill;

import java.util.Base64;
import java.util.Random;

public class ComonUtills {
    public static int generatedCode() {
        return new Random().nextInt(900000) + 100000;
    }

    public static boolean confirmPassword(String password, String preepassword){
        return password.equals(preepassword);
    }

    public static String encode(String str){
        return Base64.getEncoder().encodeToString(str.getBytes());
    }

    public static String decode(String str){
        return new String(Base64.getDecoder().decode(str));
    }
}
