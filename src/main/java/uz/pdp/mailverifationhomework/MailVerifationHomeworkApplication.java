package uz.pdp.mailverifationhomework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MailVerifationHomeworkApplication {

    public static void main(String[] args) {
        SpringApplication.run(MailVerifationHomeworkApplication.class, args);
    }

}
