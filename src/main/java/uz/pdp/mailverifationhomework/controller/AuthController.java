package uz.pdp.mailverifationhomework.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.mailverifationhomework.payload.ApiResponse;
import uz.pdp.mailverifationhomework.payload.StudentDto;
import uz.pdp.mailverifationhomework.service.AuthService;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    AuthService authService;

    @PostMapping("/register")
    public HttpEntity<?> register(@RequestBody StudentDto dto) {
        ApiResponse apiResponse = authService.registerStudent(dto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @GetMapping("/confirm")
    public HttpEntity<?> confirm(@RequestParam String email, @RequestParam Integer code) {
        ApiResponse apiResponse = authService.confirm(email, code);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @GetMapping("/confirm")
    public HttpEntity<?> cancel(@RequestParam String email, @RequestParam Integer code) {
        ApiResponse apiResponse = authService.cancel(email, code);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }
}
