package uz.pdp.mailverifationhomework.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.mailverifationhomework.entity.Student;
import uz.pdp.mailverifationhomework.payload.ApiResponse;
import uz.pdp.mailverifationhomework.payload.StudentDto;
import uz.pdp.mailverifationhomework.repository.StudentRepository;
import uz.pdp.mailverifationhomework.utill.ComonUtills;

@Service
public class AuthService {
    @Autowired
    MailService mailService;

    @Autowired
    StudentRepository studentRepository;

    public ApiResponse registerStudent(StudentDto dto) {
        try {
            if (ComonUtills.confirmPassword(dto.getPassword(), dto.getConfirmPassword())) {
                int code = ComonUtills.generatedCode();
                Student student = new Student();
                student.setFirstName(dto.getFirstName());
                student.setLastName(dto.getLastName());
                student.setPassword(ComonUtills.encode(dto.getPassword()));
                student.setEmail(dto.getEmail());
                student.setPhoneNumber(dto.getPhoneNumber());
                student.setCode(code);
                if (mailService.sentHtml(code, dto)) {
                    studentRepository.save(student);
                    return new ApiResponse("You are registered succesfully, confirm your email", true);
                } else {
                    return new ApiResponse("Try again. Error in send", false);
                }
            } else {
                return new ApiResponse("Error confirm password", false);
            }
        } catch (Exception e) {
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse confirm(String email, Integer code) {
        try {
            Student byEmailAndCode = studentRepository.findByEmailAndCode(email, code);
            byEmailAndCode.setEnable(true);
            studentRepository.save(byEmailAndCode);
            return new ApiResponse("Ok", true);
        } catch (Exception e) {
            e.printStackTrace();
            return new ApiResponse("Error", false);
        }
    }

    public ApiResponse cancel(String email, Integer code) {
        try {
            Student byEmailAndCode = studentRepository.findByEmailAndCode(email, code);
            studentRepository.delete(byEmailAndCode);
            return new ApiResponse("Ok", true);
        } catch (Exception e) {
            e.printStackTrace();
            return new ApiResponse("Error", false);
        }
    }
}
