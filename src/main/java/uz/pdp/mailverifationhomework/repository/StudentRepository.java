package uz.pdp.mailverifationhomework.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.mailverifationhomework.entity.Student;

public interface StudentRepository extends JpaRepository<Student, Integer> {
    Student findByEmailAndCode(String email, int code);
}
